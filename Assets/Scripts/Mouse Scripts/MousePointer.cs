﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.XR;

public class MousePointer : MonoBehaviour
{

    [SerializeField]
    Camera cam;
    public Camera Reticle { get; set; }

    [SerializeField]
    GameObject browser;
    public GameObject Browser { get; set; }
    public int sensetivity = 20;
    public int clickDelayMillis = 100;

    GameObject selectedObj;

    Vector3 offset = new Vector3(0, 0, -0.1f);
    Vector3 forward = new Vector3(0, 0, 0.1f);
    bool pressed = false;
    bool dragging = false;
    long timestampPress;


    void Update()
    {
        Debug.LogWarning(Input.GetAxis("Mouse X") + " " + Input.GetAxis("Mouse Y"));
        transform.position += new Vector3(Input.GetAxis("Mouse X") * 0.015f * sensetivity, Input.GetAxis("Mouse Y") * 0.015f * sensetivity, 0);


        PointerEventData eventData = new PointerEventData(EventSystem.current);
        RaycastHit hit;

        if (Input.GetMouseButtonDown(0))
             timestampPress = DateTimeOffset.Now.ToUnixTimeMilliseconds();

        if (Input.GetMouseButtonUp(0))
        {
            Debug.Log("Released + " + timestampPress);
            Physics.Raycast(transform.position + offset, forward, out hit);

            eventData.position = cam.WorldToScreenPoint(hit.point);

            if (DateTimeOffset.Now.ToUnixTimeMilliseconds() - timestampPress <= clickDelayMillis)
            {
                Debug.Log("Click! = " + timestampPress);

                if (selectedObj != null)
                    ExecuteEvents.Execute(selectedObj.name == "Canvas" ? browser : selectedObj, eventData, ExecuteEvents.pointerClickHandler);
            }
            else
            {
                if (dragging)
                    ExecuteEvents.Execute(browser, eventData, ExecuteEvents.endDragHandler);
                dragging = false;
                ExecuteEvents.Execute(browser, eventData, ExecuteEvents.pointerUpHandler);
            }

            pressed = false;
            timestampPress = long.MaxValue;
        }


        if (Input.GetMouseButton(0))
        { 
            if (Physics.Raycast(transform.position + offset, forward, out hit))
            {
                eventData.position = cam.WorldToScreenPoint(hit.point);
                selectedObj = hit.transform.gameObject;

                if (DateTimeOffset.Now.ToUnixTimeMilliseconds() - timestampPress < clickDelayMillis)
                    return;

                if (selectedObj != null)
                {
                    switch (selectedObj.name)
                    {
                        case "Canvas":
                            if (!pressed)
                            {
                                Debug.Log("Start Hold + " + timestampPress);

                                ExecuteEvents.Execute(browser, eventData, ExecuteEvents.pointerDownHandler);
                                ExecuteEvents.Execute(browser, eventData, ExecuteEvents.beginDragHandler);
                            }
                            else
                            {
                                Debug.Log("Dragging + " + timestampPress);
                                dragging = true;
                                ExecuteEvents.Execute(browser, eventData, ExecuteEvents.dragHandler);
                            }
                            selectedObj = browser;
                            break;
                        default:
                            ExecuteEvents.Execute(selectedObj, eventData, ExecuteEvents.pointerClickHandler);
                            selectedObj = null;
                            return;
                    }
                }
            }
            pressed = true;
            Debug.Log("Pressed! +_" + timestampPress);
        }        
    }
}
