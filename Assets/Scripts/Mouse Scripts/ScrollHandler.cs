﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Vuplex.WebView;

public class ScrollHandler : MonoBehaviour
{
    [SerializeField]
    GameObject browser;
    public GameObject Browser { get; set; }

    [SerializeField]
    CanvasWebViewPrefab prefab;
    public CanvasWebViewPrefab Prefab { get; set; }

    void Start()
    {
        prefab.DragMode = DragMode.DragWithinPage;
    }

    void Update()
    {
        if (Input.GetAxis("Mouse ScrollWheel") != 0f)
        {
            var eventData = new PointerEventData(EventSystem.current);
            eventData.scrollDelta = new Vector2(0, Input.GetAxis("Mouse ScrollWheel") / 10);
            ExecuteEvents.Execute(browser, eventData, ExecuteEvents.scrollHandler);
        }
    }
}
