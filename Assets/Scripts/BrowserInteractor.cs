﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuplex.WebView;

public class BrowserInteractor : MonoBehaviour
{
    public CanvasWebViewPrefab prefab;

    IWebView view;

    private void Start()
    {
        view = prefab.WebView;
    }

    public void DragPrefab(Vector2 delta, Vector2 point)
    {
        view.Scroll(delta, point);
    }

    public void Click(Vector2 point)
    {
        view.Click(point);
    }
}
