﻿﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuplex.WebView;

public class HeadGazePointer : BaseEventPointer
{
    public Camera mainCamera;
    public GameObject webView;
    public HandGestureDetector detector;

    public int dragThreshold;

    void Update()
    {
        try
        {
            var web = webView.GetComponent<CanvasWebViewPrefab>();
            var raycastResult = GetCurrentRaycast();

            if (detector.getPinchedFingers(1, 0.05f))
            {
                web.DragMode = DragMode.DragToScroll;
                ProcessPinch();
                return;
            }
        
            if (detector.getPinchedFingers(2, 0.05f))
            {
                web.DragMode = DragMode.DragWithinPage;
                ProcessMiddlePinch();
                return;
            }

            if (lastPinchTimestamp != -1)
            {
                var screenPos = ConvertToScreenCoords(mainCamera, raycastResult.Item1);
                if (DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastPinchTimestamp > dragThreshold && dragging)
                {
                    EndDrag(screenPos, raycastResult.Item2);
                    lastPinchTimestamp = -1;
                    return;
                }

                lastPinchTimestamp = -1;
                Click(ConvertToScreenCoords(mainCamera, raycastResult.Item1), raycastResult.Item2);
            }
        
            LocateObject();
        }
        catch (Exception)
        {
            Console.WriteLine("Null pointer");
        }
    }

    void ProcessPinch()
    {
        if (lastPinchTimestamp == -1)
            lastPinchTimestamp = DateTimeOffset.Now.ToUnixTimeMilliseconds();

        var raycastResult = GetCurrentRaycast();

        if (dragging)
        {
            var change = detector.getFingerXYOffset(1);
            Debug.Log(change);
            var raycastPos = lastPinchedRaycast - change;
            var screenPos = ConvertToScreenCoords(mainCamera, raycastPos);
            LocateObject(raycastPos);
            if (change.x < 0.05 && change.y < 0.05 && change.z < 0.05)
            {
                RightClick(ConvertToScreenCoords(mainCamera, raycastResult.Item1), raycastResult.Item2);
            }
            Drag(screenPos, raycastResult.Item2);
        }

        else if (DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastPinchTimestamp > dragThreshold)
        {
            var screenPos = ConvertToScreenCoords(mainCamera, raycastResult.Item1);
            lastPinchedRaycast = raycastResult.Item1;
            detector.setInitialPinch();
            BeginDrag(screenPos, raycastResult.Item2);
        }
    }
    
        void ProcessMiddlePinch()
        {
            if (lastPinchTimestamp == -1)
                lastPinchTimestamp = DateTimeOffset.Now.ToUnixTimeMilliseconds();
    
            var raycastResult = GetCurrentRaycast();
    
            if (dragging)
            {
                var raycastPos = lastPinchedRaycast - detector.getFingerXYOffset(1);
                var screenPos = ConvertToScreenCoords(mainCamera, raycastPos);
                LocateObject(raycastPos);
                Drag(screenPos, raycastResult.Item2);
            }
    
            else if (DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastPinchTimestamp > dragThreshold)
            {
                var screenPos = ConvertToScreenCoords(mainCamera, raycastResult.Item1);
                lastPinchedRaycast = raycastResult.Item1;
                detector.setInitialPinch();
                BeginDrag(screenPos, raycastResult.Item2);
            }
        }

    void LocateObject(Vector3 position = new Vector3())
    {
        if (position.magnitude == 0)
            transform.position = GetCurrentRaycast().Item1;
        else
            transform.position = position;
    }

    (Vector3, GameObject) GetCurrentRaycast()
    {
        RaycastHit info;
        Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out info);

        return (info.point, info.transform.gameObject);
    }
}
