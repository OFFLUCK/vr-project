﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReticlePointer : MonoBehaviour
{
    [SerializeField]

    private Camera cam;
    public Camera Cam { get; set; }

    Vector3 tr = new Vector3(0, -5, 0);


    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit))
            transform.position = hit.point;
        else
            transform.position = tr;
    }
}
