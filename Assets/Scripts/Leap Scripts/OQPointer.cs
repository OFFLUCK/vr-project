﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OQPointer : BaseEventPointer
{
    public Camera mainCamera;
    public GameObject webView;
    public HandGestureDetector detector;

    public int dragThreshold;

    Vector3 oculusOffset = new Vector3(0.15f, -0.5f, -0.2f);

    Vector3 getFingersMidPoint
    {
        get { return (detector.getFingerPosition(1) + detector.getFingerPosition(0)) / 2; }
    }

    void Update()
    {
        var raycastResult = GetCurrentRaycast();

        if (detector.getPinchedFingers(1, 0.05f))
        {
            ProcessPinch();
            return;
        }

        else if (lastPinchTimestamp != -1)
        {
            var screenPos = ConvertToScreenCoords(mainCamera, raycastResult.Item1);
            if (DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastPinchTimestamp > dragThreshold && dragging)
            {
                EndDrag(screenPos, raycastResult.Item2);
                lastPinchTimestamp = -1;
                return;
            }

            lastPinchTimestamp = -1;
            Click(ConvertToScreenCoords(mainCamera, raycastResult.Item1), raycastResult.Item2);
        }

        LocateObject();
    }

    void ProcessPinch()
    {
        if (lastPinchTimestamp == -1)
            lastPinchTimestamp = DateTimeOffset.Now.ToUnixTimeMilliseconds();

        var raycastResult = GetCurrentRaycast();

        if (dragging)
        {
            var raycastPos = lastPinchedRaycast - detector.getFingerXYOffset(1);
            var screenPos = ConvertToScreenCoords(mainCamera, raycastPos);
            LocateObject(raycastPos);
            Drag(screenPos, raycastResult.Item2);
        }

        else if (DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastPinchTimestamp > dragThreshold)
        {
            var screenPos = ConvertToScreenCoords(mainCamera, raycastResult.Item1);
            lastPinchedRaycast = raycastResult.Item1;
            detector.setInitialPinch();
            BeginDrag(screenPos, raycastResult.Item2);
        }
    }

    void LocateObject(Vector3 position = new Vector3())
    {
        if (position.magnitude == 0)
            transform.position = GetCurrentRaycast().Item1;
        else
            transform.position = position;
    }

    (Vector3, GameObject) GetCurrentRaycast()
    {
        RaycastHit info;
        var sourcePoint = mainCamera.transform.position + oculusOffset;
        Physics.Raycast(sourcePoint, getFingersMidPoint - sourcePoint, out info);


        Debug.Log(info.point.x);
        if (info.transform != null)
            return (info.point, info.transform.gameObject);

        return (info.point, null);
    }
}
