/**
* Copyright (c) 2020 Vuplex Inc. All rights reserved.
*
* Licensed under the Vuplex Commercial Software Library License, you may
* not use this file except in compliance with the License. You may obtain
* a copy of the License at
*
*     https://vuplex.com/commercial-library-license
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
#if UNITY_STANDALONE_OSX
#pragma warning disable CS0618
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Callbacks;
using UnityEngine.Rendering;

namespace Vuplex.WebView {

    public class MacBuildScript : IPreprocessBuild {

        public int callbackOrder { get { return 0; } }

        public void OnPreprocessBuild(BuildTarget buildTarget, string buildPath) {

            if (buildTarget != BuildTarget.StandaloneOSX) {
                return;
            }
            var selectedGraphicsApi = PlayerSettings.GetGraphicsAPIs(buildTarget)[0];
            var error = Utils.GetGraphicsApiErrorMessage(selectedGraphicsApi, new GraphicsDeviceType[] { GraphicsDeviceType.Metal });
            if (error != null) {
                throw new BuildFailedException(error);
            }
        }

        /// <summary>
        /// Deletes all of the .meta files added by Unity, because they cause a codesign mismatch
        /// which causes app notarization to fail.
        /// </summary>
        [PostProcessBuild(700)]
        public static void OnPostProcessBuild(BuildTarget target, string pathToBuiltProject) {

            if (target != BuildTarget.StandaloneOSX) {
                return;
            }
            var metaFiles = Directory.GetFiles(pathToBuiltProject, "*.meta", SearchOption.AllDirectories);
            foreach (var file in metaFiles) {
                File.Delete(file);
            }
        }
    }
}
#endif // UNITY_STANDALONE_OSX
