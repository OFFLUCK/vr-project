/**
* Copyright (c) 2020 Vuplex Inc. All rights reserved.
*
* Licensed under the Vuplex Commercial Software Library License, you may
* not use this file except in compliance with the License. You may obtain
* a copy of the License at
*
*     https://vuplex.com/commercial-library-license
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN || UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;

#if NET_4_6 || NET_STANDARD_2_0
    using System.Threading.Tasks;
#endif

namespace Vuplex.WebView {

    /// <summary>
    /// The base `IWebView` implementation used by 3D WebView for Windows and macOS.
    /// This class also includes extra methods for Standalone-specific functionality.
    /// It also implements the `IWithMovablePointer` and `IWithPointerDownAndUp`
    /// interfaces, so it supports hover and drag-and-drop interactions.
    /// </summary>
    public abstract class StandaloneWebView : BaseWebView,
                                              IWithKeyModifiers,
                                              IWithMovablePointer,
                                              IWithPointerDownAndUp,
                                              IWithPopups {

        public event EventHandler<PopupRequestedEventArgs> PopupRequested;

        public static new void ClearAllData() {

            var pluginIsInitialized = WebView_pluginIsInitialized();
            if (pluginIsInitialized) {
                _throwAlreadyInitializedException("clear the browser data", "ClearAllData");
            }
            var cachePath = _getCachePath();
            if (Directory.Exists(cachePath)) {
                Directory.Delete(cachePath, true);
            }
        }

        public override void Copy() {

            _assertValidState();
            WebView_copy(_nativeWebViewPtr);
        }

        public override void Cut() {

            _assertValidState();
            WebView_cut(_nativeWebViewPtr);
        }

        /// <summary>
        /// Enables remote debugging with Chrome DevTools on the given port.
        /// Note that this method can only be called prior to
        /// initializing any webviews.
        /// </summary>
        /// <remarks>
        /// - For example, if you provide 8080 as the `portNumber`, you can navigate to
        /// `http://localhost:8080 `in Chrome to see a list of webviews to inspect.
        /// - For more information on debugging, please see
        /// [this support article](https://support.vuplex.com/articles/how-to-debug-web-content#standalone).
        /// </remarks>
        /// <param name="portNumber">Port number in the range 1024 - 65535.</param>
        public static void EnableRemoteDebugging(int portNumber) {

            if (!(1024 <= portNumber && portNumber <= 65535)) {
                throw new ArgumentException(string.Format("The given port number ({0}) is not in the range from 1024 to 65535.", portNumber));
            }
            var success = WebView_enableRemoteDebugging(portNumber);
            if (!success) {
                _throwAlreadyInitializedException("enable remote debugging", "EnableRemoteDebugging");
            }
        }

    #if NET_4_6 || NET_STANDARD_2_0
        /// <summary>
        /// Gets the cookie that matches the given URL and cookie name, or
        /// null if no cookie matches. This method can only be called after
        /// one or more webviews have been initialized.
        /// </summary>
        public static Task<Cookie> GetCookie(string url, string cookieName) {

            var task = new TaskCompletionSource<Cookie>();
            GetCookie(url, cookieName, task.SetResult);
            return task.Task;
        }
    #endif

        /// <summary>
        /// Like the other version of `GetCookie()`, except it uses a callback
        /// instead of a `Task` in order to be compatible with legacy .NET.
        /// </summary>
        public static void GetCookie(string url, string cookieName, Action<Cookie> callback) {

            var pluginIsInitialized = WebView_pluginIsInitialized();
            if (!pluginIsInitialized) {
                throw new InvalidOperationException("On Windows and macOS, GetCookie() can only be called when the Chromium process is running (i.e. after a webview is initialized).");
            }
            var resultCallbackId = Guid.NewGuid().ToString();
            _pendingGetCookieResultCallbacks[resultCallbackId] = callback;
            WebView_getCookie(url, cookieName, resultCallbackId);
        }

        /// <see cref="IWithKeyModifiers"/>
        public void HandleKeyboardInput(string key, KeyModifier keyModifier) {

            _assertValidState();
            WebView_handleKeyboardInputWithModifiers(_nativeWebViewPtr, key, (int)keyModifier);
        }

        /// <summary>
        /// The native plugin invokes this method.
        /// </summary>
        public void HandlePopup(string message) {

            var handler = PopupRequested;
            if (handler == null) {
                return;
            }
            var components = message.Split(new char[] { ',' }, 2);
            var url = components[0];
            var popupBrowserId = components[1];

            if (popupBrowserId.Length == 0) {
                handler(this, new PopupRequestedEventArgs(url, null));
                return;
            }
            var popupWebView = _instantiate();
            Dispatcher.RunOnMainThread(() => {
                Web.CreateTexture(1, 1, texture => {
                    // Use the same dimensions as the current webview.
                    popupWebView._initPopup(texture, _width, _height, popupBrowserId);
                    handler(this, new PopupRequestedEventArgs(url, popupWebView as IWebView));
                });
            });
        }

        public override void Init(Texture2D viewportTexture, float width, float height, Texture2D videoTexture) {

            base.Init(viewportTexture, width, height, videoTexture);
            _nativeWebViewPtr = WebView_new(gameObject.name, _nativeWidth, _nativeHeight, null);
            if (_nativeWebViewPtr == IntPtr.Zero) {
                throw new WebViewUnavailableException("Failed to instantiate a new webview. This could indicate that you're using an expired trial version of 3D WebView.");
            }
        }

        /// <see cref="IWithMovablePointer"/>
        public void MovePointer(Vector2 point) {

            _assertValidState();
            int nativeX = (int) (point.x * _nativeWidth);
            int nativeY = (int) (point.y * _nativeHeight);
            WebView_movePointer(_nativeWebViewPtr, nativeX, nativeY);
        }

        public override void Paste() {

            _assertValidState();
            WebView_paste(_nativeWebViewPtr);
        }

        /// <see cref="IWithPointerDownAndUp"/>
        public void PointerDown(Vector2 point) {

            _assertValidState();
            int nativeX = (int) (point.x * _nativeWidth);
            int nativeY = (int) (point.y * _nativeHeight);
            WebView_pointerDown(_nativeWebViewPtr, nativeX, nativeY);
        }

        /// <see cref="IWithPointerDownAndUp"/>
        public void PointerUp(Vector2 point) {

            _assertValidState();
            int nativeX = (int) (point.x * _nativeWidth);
            int nativeY = (int) (point.y * _nativeHeight);
            WebView_pointerUp(_nativeWebViewPtr, nativeX, nativeY);
        }

        public override void SelectAll() {

            _assertValidState();
            WebView_selectAll(_nativeWebViewPtr);
        }

        /// <summary>
        /// By default, web pages cannot access the device's
        /// camera or microphone via JavaScript.
        /// Invoking `SetAudioAndVideoCaptureEnabled(true)` allows
        /// **all web pages** to access the camera and microphone.
        /// </summary>
        /// <remarks>
        /// This is useful, for example, to enable WebRTC support.
        /// This method can only be called prior to initializing any webviews.
        /// </remarks>
        public static void SetAudioAndVideoCaptureEnabled(bool enabled) {

            var success = WebView_setAudioAndVideoCaptureEnabled(enabled);
            if (!success) {
                _throwAlreadyInitializedException("enable audio and video capture", "SetAudioAndVideoCaptureEnabled");
            }
        }

        /// <summary>
        /// Sets additional command line arguments to pass to Chromium.
        /// <summary>
        /// <remarks>
        /// [Here's an unofficial list of Chromium command line arguments](https://peter.sh/experiments/chromium-command-line-switches/).
        /// This method can only be called prior to initializing any webviews.
        /// </remarks>
        /// <example>
        /// StandaloneWebView.SetCommandLineArguments("--ignore-certificate-errors --disable-web-security");
        /// </example>
        public static void SetCommandLineArguments(string args) {

            var success = WebView_setCommandLineArguments(args);
            if (!success) {
                _throwAlreadyInitializedException("set command line arguments", "SetCommandLineArguments");
            }
        }

        /// <summary>
        /// By default, the native file picker for file input elements is disabled,
        /// but it can be enabled with this method.
        /// </summary>
        public void SetNativeFileDialogEnabled(bool enabled) {

            _assertValidState();
            WebView_setNativeFileDialogEnabled(_nativeWebViewPtr, enabled);
        }

        public void SetPopupMode(PopupMode popupMode) {

            WebView_setPopupMode(_nativeWebViewPtr, (int)popupMode);
        }

        /// <summary>
        /// By default, web pages cannot share the device's screen
        /// via JavaScript. Invoking `SetScreenSharingEnabled(true)` allows
        /// **all web pages** to share the screen.
        /// </summary>
        /// <remarks>
        /// The screen that is shared is the default screen, and there isn't currently
        /// support for sharing a different screen or a specific application window.
        /// Also, this method can only be called prior to initializing any webviews.
        /// </remarks>
        public static void SetScreenSharingEnabled(bool enabled) {

            var success = WebView_setScreenSharingEnabled(enabled);
            if (!success) {
                _throwAlreadyInitializedException("enable or disable screen sharing", "SetScreenSharingEnabled");
            }
        }

        public static new void SetStorageEnabled(bool enabled) {

            var cachePath = enabled ? _getCachePath() : "";
            var success = WebView_setCachePath(cachePath);
            if (!success) {
                _throwAlreadyInitializedException("enable or disable storage", "SetStorageEnabled");
            }
        }

        /// <summary>
        /// Sets the target web frame rate. The default is 60. Specifying a target frame
        /// rate of 0 disables the frame rate limit. This method can only be called prior
        /// to initializing any webviews.
        /// </summary>
        public static void SetTargetFrameRate(uint targetFrameRate) {

            var success = WebView_setTargetFrameRate(targetFrameRate);
            if (!success) {
                _throwAlreadyInitializedException("set the target frame rate", "SetTargetFrameRate");
            }
        }

        /// <summary>
        /// Sets the zoom level to the specified value. Specify `0.0` to reset the zoom level.
        /// </summary>
        public void SetZoomLevel(float zoomLevel) {

            _assertValidState();
            WebView_setZoomLevel(_nativeWebViewPtr, zoomLevel);
        }

        public static void TerminatePlugin() {

            WebView_terminatePlugin();
        }

        delegate void GetCookieCallback(string requestId, string serializedCookie);
        delegate void UnitySendMessageFunction(string gameObjectName, string methodName, string message);

        static Dictionary<string, Action<Cookie>> _pendingGetCookieResultCallbacks = new Dictionary<string, Action<Cookie>>();

        protected static string _getCachePath() {

            // Only `Path.Combine(string, string)` is available in .NET 2.0.
            return Path.Combine(Application.persistentDataPath, Path.Combine("Vuplex.WebView", "chromium-cache"));
        }

        protected override Texture2D _getReadableTextureWithCorrectOrientation() {

            var texture = base._getReadableTexture();
            // Flip the texture vertically, since DefaultViewportMaterial normally takes care of that.
            var originalPixels = texture.GetPixels();
            var flippedPixels = new Color[originalPixels.Length];
            for (var row = 0; row < texture.height - 1; row++) {
                var sourceIndex = row * texture.width;
                var destinationIndex = ((texture.height - 1) - row) * texture.width;
                Array.Copy(originalPixels, sourceIndex, flippedPixels, destinationIndex, texture.width);
            }
            texture.SetPixels(flippedPixels);
            // We apply the changes to our new texture
            texture.Apply();
            return  texture;
        }

        [AOT.MonoPInvokeCallback(typeof(GetCookieCallback))]
        static void _handleGetCookieResult(string resultCallbackId, string serializedCookie) {

            var callback = _pendingGetCookieResultCallbacks[resultCallbackId];
            _pendingGetCookieResultCallbacks.Remove(resultCallbackId);
            var cookie = Cookie.FromJson(serializedCookie);
            callback(cookie);
        }

        void _initPopup(Texture2D viewportTexture, float width, float height, string popupId) {

            base.Init(viewportTexture, width, height, null);
            _nativeWebViewPtr = WebView_new(gameObject.name, _nativeWidth, _nativeHeight, popupId);
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        static void _initializePlugin() {

            // The generic `GetFunctionPointerForDelegate<T>` is unavailable in .NET 2.0.
            var sendMessageFunction = Marshal.GetFunctionPointerForDelegate((UnitySendMessageFunction)_unitySendMessage);
            WebView_setSendMessageFunction(sendMessageFunction);
            WebView_setCookieCallbacks(
                Marshal.GetFunctionPointerForDelegate((GetCookieCallback)_handleGetCookieResult)
            );
            SetStorageEnabled(true); // cache, cookies, and storage are enabled by default
        }

        protected abstract StandaloneWebView _instantiate();

        static void _throwAlreadyInitializedException(string action, string methodName) {

            var message = String.Format("Unable to {0}, because a webview has already been created. On Windows and macOS, {1}() can only be called prior to initializing any webviews.", action, methodName);
            throw new InvalidOperationException(message);
        }

        [AOT.MonoPInvokeCallback(typeof(UnitySendMessageFunction))]
        static void _unitySendMessage(string gameObjectName, string methodName, string message) {

            Dispatcher.RunOnMainThread(() => {
                var gameObj = GameObject.Find(gameObjectName);
                if (gameObj == null) {
                    Debug.LogErrorFormat("Unable to send the message, because there is no GameObject named '{0}'", gameObjectName);
                    return;
                }
                gameObj.SendMessage(methodName, message);
            });
        }

        [DllImport(_dllName)]
        static extern bool WebView_enableRemoteDebugging(int portNumber);

        [DllImport(_dllName)]
        static extern void WebView_copy(IntPtr webViewPtr);

        [DllImport(_dllName)]
        static extern void WebView_cut(IntPtr webViewPtr);

        [DllImport(_dllName)]
        static extern void WebView_getCookie(string url, string name, string resultCallbackId);

        [DllImport(_dllName)]
        static extern void WebView_handleKeyboardInputWithModifiers(IntPtr webViewPtr, string key, int modifiers);

        [DllImport(_dllName)]
        static extern IntPtr WebView_new(string gameObjectName, int width, int height, string popupBrowserId);

        [DllImport (_dllName)]
        static extern void WebView_movePointer(IntPtr webViewPtr, int x, int y);

        [DllImport(_dllName)]
        static extern void WebView_paste(IntPtr webViewPtr);

        [DllImport(_dllName)]
        static extern bool WebView_pluginIsInitialized();

        [DllImport (_dllName)]
        static extern void WebView_pointerDown(IntPtr webViewPtr, int x, int y);

        [DllImport (_dllName)]
        static extern void WebView_pointerUp(IntPtr webViewPtr, int x, int y);

        [DllImport(_dllName)]
        static extern void WebView_selectAll(IntPtr webViewPtr);

        [DllImport(_dllName)]
        static extern bool WebView_setAudioAndVideoCaptureEnabled(bool enabled);

        [DllImport(_dllName)]
        static extern bool WebView_setCachePath(string cachePath);

        [DllImport(_dllName)]
        static extern bool WebView_setCommandLineArguments(string args);

        [DllImport(_dllName)]
        static extern int WebView_setCookieCallbacks(IntPtr getCookieCallback);

        [DllImport(_dllName)]
        static extern int WebView_setSendMessageFunction(IntPtr sendMessageFunction);

        [DllImport(_dllName)]
        static extern void WebView_setNativeFileDialogEnabled(IntPtr webViewPtr, bool enabled);

        [DllImport(_dllName)]
        static extern void WebView_setPopupMode(IntPtr webViewPtr, int popupMode);

        [DllImport(_dllName)]
        static extern bool WebView_setScreenSharingEnabled(bool enabled);

        [DllImport(_dllName)]
        static extern bool WebView_setTargetFrameRate(uint targetFrameRate);

        [DllImport(_dllName)]
        static extern void WebView_setZoomLevel(IntPtr webViewPtr, float zoomLevel);

        [DllImport(_dllName)]
        static extern void WebView_terminatePlugin();
    }
}
#endif
